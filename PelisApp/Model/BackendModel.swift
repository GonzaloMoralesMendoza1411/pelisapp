//
//  BackendModel.swift
//  PelisApp
//
//  Created by Gonzalo Morales on 17/11/20.
//

import UIKit

struct Message: Decodable {
    let message: String
}

struct configuration: Decodable {
    let images: objectConfiguration
    let change_keys: [String]
}

struct objectConfiguration: Decodable {
    let base_url: String
    let secure_base_url: String
    let backdrop_sizes: [String]
    let logo_sizes: [String]
    let poster_sizes: [String]
    let profile_sizes: [String]
    let still_sizes: [String]
}

struct movieList: Decodable {
    let page: Int
    let results: [arrayMovies]
}

struct arrayMovies: Decodable {
    let poster_path: String
    let adult: Bool
    let overview: String
    let release_date: String
    let genre_ids: [Int]
    let id: Int
    let original_title: String
    let original_language: String
    let title: String
    let backdrop_path: String
    let popularity: Double
    let vote_count: Int
    let video: Bool
    let vote_average: Double
}

struct detailMovie: Decodable {
    let adult: Bool
    let backdrop_path: String
    let belongs_to_collection: String?
    let budget: Int
    let genres: [arrayGenres]
    let homepage: String
    let id: Int
    let imdb_id: String
    let original_language: String
    let original_title: String
    let overview: String
    let popularity: Double
    let poster_path: String
    let production_companies: [arrayProductionCompanies]
    let production_countries: [arrayproductionCountries]
    let release_date: String
    let revenue: Int
    let runtime: Int
    let spoken_languages: [arraySpokenLenguajes]
    let status: String
    let tagline: String
    let title: String
    let video: Bool
    let vote_average: Double
    let vote_count: Int
}

struct arrayGenres: Decodable {
    let id: Int
    let name: String
}

struct arrayProductionCompanies: Decodable {
    let id: Int
    let logo_path: String?
    let name: String
    let origin_country: String
}

struct arrayproductionCountries: Decodable {
    let iso_3166_1: String
    let name: String
}

struct arraySpokenLenguajes: Decodable {
    let iso_639_1: String
    let name: String
}
