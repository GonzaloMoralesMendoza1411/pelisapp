//
//  BackendConnection.swift
//  PelisApp
//
//  Created by Gonzalo Morales on 17/11/20.
//

import UIKit

class BackendConnection: UIViewController {
    
    static let USE_TEST_API = false
    static let baseURLString = USE_TEST_API ? "" : "https://api.themoviedb.org/3/configuration?api_key="
    
    static let baseURLMovie = USE_TEST_API ? "": "https://api.themoviedb.org/3/movie/now_playing?api_key="
   
    static let baseURLVersioning = "\(baseURLString)"
    
    static let autentication = "634b49e294bd1ff87914e7b9d014daed"
        
    enum Endpoints {
        case configuration
        case movie
       
        var url: String {
            switch self {
            case .configuration:
                    return "\(baseURLVersioning)\(autentication)"
            case .movie:
                    return "\(baseURLMovie)\(autentication)"
            
            }
        }
    }
}

extension BackendConnection {
    
    static func getGenericData<T: Decodable>(urlString: String, completion: @escaping (T?) -> ()) {
    var request = URLRequest(url: URL(string: urlString)!)
    request.httpMethod = "GET"
    
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
       
    URLSession.shared.dataTask(with: request) { data, response, error in
    
        guard let data = data else { return }
        
        print(String(data: data, encoding: String.Encoding.utf8))
        
        do {
            
            let obj = try JSONDecoder().decode(T.self, from: data)
                
            completion(obj)
                
        } catch let jsonError {
            
            print("Failed to decode json:", jsonError)
            completion(nil)
            
        }
        
        }.resume()
    }
    
    
}
