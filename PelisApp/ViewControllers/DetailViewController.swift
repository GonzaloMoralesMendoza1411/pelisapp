//
//  DetailViewController.swift
//  PelisApp
//
//  Created by Gonzalo Morales on 17/11/20.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet var imgContent: UIImageView!
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDuration: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblQuality: UILabel!
    @IBOutlet var lblGenere: UILabel!
    @IBOutlet var lblDescription: UILabel!
    
    var linkImage: String = ""
    var titleMovie: String = ""
    var durationMovie: Int = 0
    var dateMovie: String = ""
    var qualityMovie: Double = 0.0
    var generMovie: [String] = []
    var descriptionMovie: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configScreen()
    }
    

    func configScreen(){
        let urlImage = URL(string: linkImage)
        self.imgContent.kf.setImage(with: urlImage)
        self.lblTitle.text = titleMovie
        self.lblDuration.text = "\(durationMovie) min"
        self.lblDate.text = dateMovie
        self.lblQuality.text = "\(qualityMovie)"
        self.lblGenere.text = generMovie.joined(separator: ",")
        self.lblDescription.text = descriptionMovie
    }

}


