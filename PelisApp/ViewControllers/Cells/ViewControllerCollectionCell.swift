//
//  ViewControllerCollectionCell.swift
//  PelisApp
//
//  Created by Gonzalo Morales on 17/11/20.
//

import UIKit

class ViewControllerCollectionCell: UICollectionViewCell {
    
    @IBOutlet var imgContent: UIImageView!
    @IBOutlet var viewContent: UIView!
    
    @IBOutlet var showView: UIView!
    @IBOutlet var titleShowView: UILabel!
    @IBOutlet var dateShowView: UILabel!
    @IBOutlet var qualityShowView: UILabel!
    
}
