//
//  ViewController.swift
//  PelisApp
//
//  Created by Gonzalo Morales on 17/11/20.
//

import UIKit
import Kingfisher

class ViewController: UIViewController {

    @IBOutlet var collectionView: UICollectionView!
    

    var refresher: UIRefreshControl!
    var urlConfiguration: String = ""
    var urlComplete: String = ""
    
    var arrayURL: [String] = []
    var arrayDetailsMovie: [arrayMovies] = []
    var arrayDetal: [detailMovie] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurationGet()
    }
    
    
    func configurationGet() {
            BackendConnection.getGenericData(urlString: BackendConnection.Endpoints.configuration.url) { (configuration: configuration?) in
                
                self.urlConfiguration = "\(configuration?.images.base_url ?? "nil")\(configuration?.images.backdrop_sizes.first ?? "nil")"
                self.movieListGet()
                
            }
    }
    
    
    func movieListGet() {
        BackendConnection.getGenericData(urlString: BackendConnection.Endpoints.movie.url) { (movieList: movieList?) in
            
            DispatchQueue.main.async {
                for value in movieList!.results {
                    print(value.poster_path)
                    
                    self.urlComplete = "\(self.urlConfiguration)\(value.poster_path)"
                    self.arrayURL.append(self.urlComplete)
                    print(self.arrayURL)
                    
                }
                
                if movieList != nil {
                    self.collectionView.delegate = self
                    self.collectionView.dataSource = self
                    self.collectionView.reloadData()
                    self.arrayDetailsMovie.append(contentsOf: movieList!.results)
                }
            }
        }
    }
    
    func detailMovie(idProyecto: Int) {
       // let idProyecto = "671039"
        BackendConnection.getGenericData(urlString: "https://api.themoviedb.org/3/movie/\(idProyecto)?api_key=634b49e294bd1ff87914e7b9d014daed&language=en%20US") { (detailMovie: detailMovie?) in
            
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
                vc.titleMovie = detailMovie?.title ?? "NIL"
                vc.durationMovie = detailMovie?.runtime ?? 0
                vc.dateMovie = detailMovie?.release_date ?? "NIL"
                vc.qualityMovie = detailMovie?.vote_average ?? 0.0
                for value in detailMovie!.genres {
                    vc.generMovie.append(value.name)
                }
                vc.descriptionMovie = detailMovie?.overview ?? "NIL"
                let linkImage = "\(self.urlConfiguration)\(detailMovie?.poster_path ?? "NIL")"
                vc.linkImage = linkImage
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }

}
extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.arrayURL.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ViewControllerCollectionCell", for: indexPath) as! ViewControllerCollectionCell
        let data = self.arrayURL[indexPath.row]
        let url = URL(string: data)
        print(data)
        cell.imgContent.kf.setImage(with: url)
        cell.imgContent.layer.masksToBounds =  true
        cell.imgContent.layer.cornerRadius = cell.imgContent.frame.height / 10.0
        cell.imgContent.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let data = self.arrayDetailsMovie[indexPath.row]
        detailMovie(idProyecto: data.id)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width / 2.1, height: view.frame.height / 3)
    }
    
    
}

